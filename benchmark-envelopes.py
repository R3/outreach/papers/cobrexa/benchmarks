#!/usr/bin/env python

import cobra, sys, os
from time import time

if len(sys.argv) != 5:
    sys.exit(1)

tag=sys.argv[1]
seed=int(sys.argv[2])
n_models=int(sys.argv[3])
n_breaks=int(sys.argv[4])
n_workers=int(os.environ['SLURM_CPUS_PER_TASK'])
modelfile = "data/model-%s-%d-%d.json" % (tag, n_models, seed)

logHandle = open("logs/log_envelope.py-%s-%d-%d-%d-%d" % (tag, n_models, n_workers, n_breaks, seed), "w")

def log(op, *args):
    logHandle.write("\t".join([op, tag, str(n_models), str(n_workers), str(n_breaks), str(seed)]+list(map(str,args))) + "\n")
    logHandle.flush()

t0 = time()
m = cobra.io.load_json_model(modelfile)
td = time() - t0
log("envelope_json_load.py", td)

avail_reactions = [r.id for r in m.reactions]

interesting_substrates = ["sucr", "gln_L", "gal", "glu_L", "glc_D", "xyl_D", "arab_L"]
interesting_exchanges = ["@M_" + s + "(e)" for s in interesting_substrates]
n_exchanges=3
exchanges=[i for i in interesting_exchanges if i in avail_reactions]
if len(exchanges)<n_exchanges:
    print("not enough exchange reactions found")
    sys.exit(2)

exchanges=exchanges[0:n_exchanges]

log("envelope_exchanges.py", ",".join(exchanges))

exchange_idxs = [i for i in range(len(avail_reactions)) if (avail_reactions[i] in exchanges)]

from cobra.flux_analysis import production_envelope

t0 = time()
for i in exchange_idxs:
    m.reactions[i].lower_bound=0.0
    m.reactions[i].upper_bound=100.0

pe = production_envelope(m, exchanges, points=n_breaks, carbon_sources=exchanges)
td = time() - t0
log("envelope_time.py", n_exchanges, td)
log("envelope.py", n_exchanges, ','.join(list(map(str, pe.flux_maximum))))

logHandle.close()
