#!/usr/bin/env python

import cobra, sys, os
from time import time

if len(sys.argv) != 4:
    sys.exit(1)

tag=sys.argv[1]
seed=int(sys.argv[2])
n_models=int(sys.argv[3])
n_workers=int(os.environ['SLURM_CPUS_PER_TASK'])
modelfile = "data/model-%s-%d-%d.json" % (tag, n_models, seed)

logHandle = open("logs/log_fva.py-%s-%d-%d-%d" % (tag, n_models, n_workers, seed), "w")

def log(op, *args):
    logHandle.write("\t".join([op, tag, str(n_models), str(n_workers), str(seed)]+list(map(str,args))) + "\n")
    logHandle.flush()

t0 = time()
m = cobra.io.load_json_model(modelfile)
td = time() - t0
log("fva_json_load.py", td)

fva_reactions = [r.id for r in m.reactions if "<=>" in r.id]

log("fva_n_reactions.py", len(fva_reactions))

from cobra.flux_analysis import flux_variability_analysis

t0 = time()
res = flux_variability_analysis(m, fva_reactions, processes=n_workers, fraction_of_optimum=0.9)
td = time() - t0
log("fva_time.py", len(fva_reactions), td)
log("variability_min.py", ','.join(list(map(str, res.minimum))))
log("variability_max.py", ','.join(list(map(str, res.maximum))))

logHandle.close()
