#!/bin/bash

req_mem() {
	case $1 in
	5|10) echo 4G ;;
	20) echo 6G ;;
	50|100) echo 8G ;;
	200|500) echo 16G ;;
	*) echo 4G ;;
	esac
}

make_prep() {
cat > prep-$2-$1.sbatch << EOF
#!/bin/bash -l
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -t $((5 + $2 / 50))
#SBATCH -J cobrexa-prep-$2-$1
#SBATCH --mem-per-cpu `req_mem $2`

julia benchmark-prep.jl agora $1 $2
EOF
}

make_envelope() {
cat > envelope-$2-$4-$3-$1.sbatch << EOF
#!/bin/bash -l
#SBATCH -n $3
#SBATCH -c 1
#SBATCH -t $(( 20 + $2 * $4 * $4 * $4 / $3 / 150 ))
#SBATCH -J cobrexa-envelope-$2-$4-$3-$1
#SBATCH --mem-per-cpu `req_mem $2`

module add math/Gurobi

julia benchmark-envelopes.jl agora $1 $2 $4
EOF

if [ $3 -le 1 ]
then cat > envelope.py-$2-$4-$3-$1.sbatch << EOF
#!/bin/bash -l
#SBATCH -n 1
#SBATCH -c $3
#SBATCH -t $(( 5 + $2 * $4 * $4 * $4 / $3 / 150 ))
#SBATCH -J cobrapy-envelope-$2-$4-$3-$1
#SBATCH --mem `req_mem $2`

module add math/Gurobi
module add lang/Python
source \$HOME/env-cobrapy/bin/activate

./benchmark-envelopes.py agora $1 $2 $4
EOF
fi
}

make_fva() {
cat > fva-$2-$3-$1.sbatch << EOF
#!/bin/bash -l
#SBATCH -n $3
#SBATCH -c 1
#SBATCH -t $(( ( $2 * 300 / $3 + 600 + 3 * $3 ) / 60 ))
#SBATCH -J cobrexa-fva-$2-$3-$1
#SBATCH --mem-per-cpu `req_mem $2`

module add math/Gurobi

julia benchmark-fva.jl agora $1 $2
EOF

if [ $3 -le 28 ]
then cat > fva.py-$2-$3-$1.sbatch << EOF
#!/bin/bash -l
#SBATCH -n 1
#SBATCH -c $3
#SBATCH -t $(( ( $2 * 300 / $3 + 600 + 3 * $3 ) / 60 ))
#SBATCH -J cobrapy-fva-$2-$3-$1
#SBATCH --mem `req_mem $2`

module add math/Gurobi
module add lang/Python
source \$HOME/env-cobrapy/bin/activate

./benchmark-fva.py agora $1 $2
EOF
fi
}

seeds=`seq 1 3`
sizes="5 10 20 50"
envsizes="7 10 14"
nworkerss="1 4 16 64 256"

for seed in $seeds ; do
	for size in $sizes ; do
		make_prep $seed $size
		for nworkers in $nworkerss ; do
			for envsize in $envsizes ; do
				make_envelope $seed $size $nworkers $envsize
			done
			make_fva $seed $size $nworkers
		done
	done
done
