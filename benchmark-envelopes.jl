
using Distributed, COBREXA, Serialization, ClusterManagers, Gurobi

using Logging
global_logger(ConsoleLogger(stderr, Logging.Debug))

if length(ARGS) != 4
    @error "bad arguments"
    exit(1)
end

tag = ARGS[1]
seed = parse(Int, ARGS[2])
n_models = parse(Int, ARGS[3])
n_breaks = parse(Int, ARGS[4])
n_workers = parse(Int, ENV["SLURM_NTASKS"])
name = "$tag-$n_models-$n_breaks-$seed"

logHandle = open("logs/log_envelope-$tag-$n_models-$n_workers-$n_breaks-$seed", "w")

function log(op, args...)
    println(
        logHandle,
        join(
            vcat(
                [
                    op,
                    tag,
                    string(seed),
                    string(n_models),
                    string(n_workers),
                    string(n_breaks),
                ],
                [string(a) for a in args],
            ),
            "\t",
        ),
    )
    flush(logHandle)
end

addprocs_slurm(n_workers, topology=:master_worker)

@info "Distribution:" workers = workers()

model_file = "data/model-$tag-$n_models-$seed.json"

#speed of COBRApy-like model loading+building at once
test_load(fn) = make_optimization_model(load_model(fn), Gurobi.Optimizer)
t=@elapsed test_load(model_file) #precompile
t=@elapsed test_load(model_file)
log("envelope_json_load_make", t)

t = @elapsed m = load_model(StandardModel, model_file)
log("envelope_json_load", t)

interesting_substrates = ["sucr", "gln_L", "gal", "glu_L", "glc_D", "xyl_D", "arab_L"]
interesting_exchanges = "@M_" .* interesting_substrates .* "(e)"

n_exchanges = 3

exchanges = filter(in(reactions(m)), interesting_exchanges)
if length(interesting_exchanges) < n_exchanges
    @error "not enough exchange reactions" exchanges
    exit(2)
end
exchanges = exchanges[begin:n_exchanges]

log("envelope_exchanges", join(exchanges, ","))

for e in exchanges
    m.reactions[e].lb = 0.0
    m.reactions[e].ub = 100.0
end

t = @elapsed sm = serialize_model(m, "data/sm-$tag-$n_models-$n_workers-$seed")
log("serialize", t)

@everywhere using COBREXA, Gurobi

#precompile
@info "precompiling"
t = @elapsed begin
    ridxs = Int.(indexin(exchanges[begin:begin], reactions(m)))
    variability = flux_variability_analysis(
        sm,
        ridxs,
        Gurobi.Optimizer;
        workers = workers(),
        modifications = [silence, change_optimizer_attribute("Method", 0), change_optimizer_attribute("Quad", 0)],
        optimal_objective_value = 0, # simulate COBRApy behavior
        bounds = _ -> (0, Inf),
    )
    objective_envelope(
        sm,
        exchanges[begin:begin],
        Gurobi.Optimizer;
        workers = workers(),
        modifications = [silence, change_optimizer_attribute("Method", 0), change_optimizer_attribute("Quad", 0)],
        lattice_args = (
            samples = n_workers,
            ranges = collect(zip(variability[:, 1], variability[:, 2])),
        ),
    )
end
log("envelope_precompile_time", t)

@info "running"
t = @elapsed begin
    ridxs = Int.(indexin(exchanges, reactions(m)))
    variability = flux_variability_analysis(
        sm,
        ridxs,
        Gurobi.Optimizer;
        workers = workers(),
        modifications = [silence, change_optimizer_attribute("Method", 0), change_optimizer_attribute("Quad", 0)],
        optimal_objective_value = 0, # simulate COBRApy behavior
        bounds = _ -> (0, Inf),
    )
    global r = objective_envelope(
        sm,
        exchanges,
        Gurobi.Optimizer;
        workers = workers(),
        modifications = [silence, change_optimizer_attribute("Method", 0), change_optimizer_attribute("Quad", 0)],
        lattice_args = (
            samples = n_breaks,
            ranges = collect(zip(variability[:, 1], variability[:, 2])),
        ),
    )
end
log("envelope_time", n_exchanges, t)
log("envelope", n_exchanges, join(r.values, ","))
close(logHandle)
