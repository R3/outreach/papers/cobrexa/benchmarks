
changeCobraSolver('gurobi','lp');

mdlSize = [5 10 20];
mdlNumber = 1:3;

solvParam = struct('feasTol', 1e-6,...
                   'optTol', 1e-6,...
                   'method', '0',...
                   'quad', 0,...
                   'threads', 1);
%% 1 CPU
poolobj = gcp('nocreate');
delete(poolobj);

times = zeros(numel(mdlNumber), numel(mdlSize));

for iSize=1:numel(mdlSize)
    for iNumber=1:numel(mdlNumber)
        filename = sprintf('data/model-agora-%d-%d.mat',...
            mdlSize(iSize),mdlNumber(iNumber));
        fprintf(['running ' filename '...\n']);
        mdl = load(filename);
        mdl = mdl.model;
        mdl = rmfield(mdl,'C');
        rxns = mdl.rxns(contains(mdl.rxns,'<=>'));
        tic;
        fluxVariability(mdl,100,'max',rxns,'threads',1);
        elap = toc
        times(iNumber,iSize) = elap;
    end
end

writematrix(["mdlN/mdlSize" mdlSize; [mdlNumber' times]],...
            'timestable_cpu1.csv');

%% 4 CPU
parpool

mdlSize = [5 10 20 50];
times = zeros(numel(mdlNumber), numel(mdlSize));

for iSize=1:numel(mdlSize)
    for iNumber=1:numel(mdlNumber)
        filename = sprintf('data/model-agora-%d-%d.mat',...
            mdlSize(iSize),mdlNumber(iNumber));
        fprintf(['running ' filename '...\n']);
        mdl = load(filename);
        mdl = mdl.model;
        mdl = rmfield(mdl,'C');
        rxns = mdl.rxns(contains(mdl.rxns,'<=>'));
        tic;
        fluxVariability(mdl,100,'max',rxns,'threads',4);
        elap = toc
        times(iNumber,iSize) = elap;
    end
end

writematrix(["mdlN/mdlSize" mdlSize; [mdlNumber' times]],...
                'timestable_cpu4.csv');

