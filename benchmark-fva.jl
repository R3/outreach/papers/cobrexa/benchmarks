
using Distributed, COBREXA, Serialization, ClusterManagers, Gurobi, Logging
global_logger(ConsoleLogger(stderr, Logging.Debug))

if length(ARGS) != 3
    @error "bad arguments"
    exit(1)
end

tag = ARGS[1]
seed = parse(Int, ARGS[2])
n_models = parse(Int, ARGS[3])
n_workers = parse(Int, ENV["SLURM_NTASKS"])
name = "$tag-$n_models-$seed"

logHandle = open("logs/log_fva-$tag-$n_models-$n_workers-$seed", "w")

function log(op, args...)
    println(
        logHandle,
        join(
            vcat(
                [
                    op,
                    tag,
                    string(seed),
                    string(n_models),
                    string(n_workers),
                ],
                [string(a) for a in args],
            ),
            "\t",
        ),
    )
    flush(logHandle)
end

addprocs_slurm(n_workers, topology=:master_worker)

@info "Distribution:" workers = workers()

t = @elapsed m = load_model(StandardModel, "data/model-$tag-$n_models-$seed.json")
log("fva_json_load", t)

ridxs = [i for (i,r) in enumerate(reactions(m)) if occursin("<=>", r)]
log("fva_n_reactions", length(ridxs))

t = @elapsed sm = serialize_model(m, "data/sm-fva-$name")
log("fva_serialize", t)

@everywhere using COBREXA, Gurobi

#precompile
@info "precompiling"
t = @elapsed begin
    flux_variability_analysis(
        sm,
        ridxs[begin:min(n_workers,length(ridxs))],
        Gurobi.Optimizer;
        workers = workers(),
        modifications = [silence, change_optimizer_attribute("Method", 0), change_optimizer_attribute("Quad", 0)],
        bounds = gamma_bounds(0.9),
    )
end
log("fva_precompile_time", t)

t = @elapsed begin
    r = flux_variability_analysis(
        m,
        ridxs,
        Gurobi.Optimizer;
        workers = workers(),
        modifications = [silence,
            change_optimizer_attribute("FeasibilityTol", 1e-6),
            change_optimizer_attribute("OptimalityTol", 1e-6),
            change_optimizer_attribute("Method", 0),
            change_optimizer_attribute("Quad", 0),
            change_optimizer_attribute("Threads", 1),
        ],
        bounds = gamma_bounds(0.9),
    )
end
log("fva_time", length(ridxs), t)
log("variability_min", join(r[:,1], ","))
log("variability_max", join(r[:,2], ","))
close(logHandle)
