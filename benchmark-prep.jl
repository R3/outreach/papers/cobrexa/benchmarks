
using SBML, StableRNGs, Serialization, COBREXA, SparseArrays

using Logging
global_logger(ConsoleLogger(stderr, Logging.Debug))

if length(ARGS) != 3
    @error "wrong arguments"
    exit(1)
end

tag = ARGS[1]
seed = parse(Int, ARGS[2])
n_models = parse(Int, ARGS[3])
name = "$tag-$n_models-$seed"

logHandle = open("logs/log_create-" * name, "w")

function log(op, args...)
    println(
        logHandle,
        join(
            vcat([op, tag, string(seed), string(n_models)], [string(a) for a in args]),
            "\t",
        ),
    )
    flush(logHandle)
end

function cleanSBMLId(x)
    x = replace(x, "__40__" => "(")
    x = replace(x, "__41__" => ")")
    x = replace(x, "__91__" => "(") #normalize to ()
    x = replace(x, "__93__" => ")")
    x
end

intMetId(fn, met) = fn * "/" * cleanSBMLId(met)
commMetId(met) = "@" * cleanSBMLId(met)
xchgRxnId(fn, met) = "@" * cleanSBMLId(met) * "<=>" * fn

mutable struct rxn
    stoi::Dict{String,Float64}
    lb::Float64
    ub::Float64
    oc::Float64
end

rxns = Dict{String,rxn}()
mets = Set{String}()
cmets = Set{String}()

r = StableRNG(seed)
files = readdir("models/")
for i = 1:length(files)-1 # knuth shuffle
    sw = rand(r, i:length(files))
    t = files[i]
    files[i] = files[sw]
    files[sw] = t
end

function load_files(fns)
    for fn in fns
        @info fn
        m = readSBML("models/" * fn)
        fn = fn[begin:end-4]

        exchange_mets = Set{String}()

        for (rid, r) in m.reactions
            if startswith(rid, "R_EX_")
                allmets = vcat(collect(keys(r.reactants)), collect(keys(r.products)))
                if length(allmets) != 1
                    @warn "problematic exchange" fn rid allmets
                else
                    push!(exchange_mets, first(allmets))
                    continue
                end
            end

            nr = rxn(Dict{String,Float64}(), r.lb[1], r.ub[1], 0.0)
            for (sid, coef) in r.reactants
                mid = intMetId(fn, sid)
                push!(mets, mid)
                nr.stoi[mid] = -coef
            end

            for (sid, coef) in r.products
                mid = intMetId(fn, sid)
                push!(mets, mid)
                nr.stoi[mid] = coef
            end

            rxns[fn*"/"*cleanSBMLId(rid)] = nr
        end

        for sid in exchange_mets
            cmi = occursin("biomass", lowercase(sid)) ? "BIOMASS" : commMetId(sid)
            push!(cmets, cmi)
            rxns[xchgRxnId(fn, sid)] =
                rxn(Dict(cmi => 1, intMetId(fn, sid) => -1), -1000.0, 1000.0, 0)
        end
    end
end

#dummy
load_files(files[begin:begin])
empty!(rxns)
empty!(mets)
empty!(cmets)

t = @elapsed load_files(files[1:n_models])

for cmet in cmets
    if cmet == "BIOMASS"
        rxns[cmet] = rxn(Dict(cmet => -1), 0.0, 1000.0, 1.0)
    else
        rxns[cmet] = rxn(
            Dict(cmet => 1),
            -1000.0,
            cmet in ["@M_o2(e)", "@M_co2(e)", "@M_h2o(e)"] ? 1000.0 : 10.0,
            0,
        )
    end
end

log("load", t)
log("community", length(rxns), length(mets), length(cmets))
log.("community_contains", files[1:n_models])

#dummy
open(f -> serialize(f, cmets), "data/cmets-" * name, "w")

t = @elapsed open(f -> serialize(f, rxns), "data/rxns-" * name, "w")
log("save_rxns", t, filesize("data/rxns-" * name))

t = @elapsed open(f -> serialize(f, mets), "data/mets-" * name, "w")
log("save_mets", t, filesize("data/mets-" * name))

t = @elapsed open(f -> serialize(f, cmets), "data/cmets-" * name, "w")
log("save_cmets", t, filesize("data/cmets-" * name))

# create the custom model structures and instance them as COBREXA MetabolicModel
mutable struct rxn
    stoi::Dict{String,Float64}
    lb::Float64
    ub::Float64
    oc::Float64
end

mutable struct mdl <: MetabolicModel
    rxns::Dict{String,rxn}
    mets::Set{String}
    cmets::Set{String}
end

COBREXA.reactions(m::mdl) = sort(collect(keys(m.rxns)))
COBREXA.n_reactions(m::mdl) = length(m.rxns)
COBREXA.metabolites(m::mdl) = sort(vcat(collect(m.mets), collect(m.cmets)))
COBREXA.n_metabolites(m::mdl) = length(m.mets) + length(m.cmets)
COBREXA.balance(m::mdl) = spzeros(n_metabolites(m))
COBREXA.stoichiometry(m::mdl) = begin
    rd = Dict(zip(reactions(m), 1:n_reactions(m)))
    md = Dict(zip(metabolites(m), 1:n_metabolites(m)))

    n_entries = 0
    for (rid, r) in m.rxns
        for _ in r.stoi
            n_entries += 1
        end
    end

    MI = Vector{Int}()
    RI = Vector{Int}()
    SV = Vector{Float64}()
    sizehint!(MI, n_entries)
    sizehint!(RI, n_entries)
    sizehint!(SV, n_entries)

    for (rid, r) in m.rxns
        for (mid, coeff) in r.stoi

            push!(MI, md[mid])
            push!(RI, rd[rid])
            push!(SV, coeff)
        end
    end
    return SparseArrays.sparse(MI, RI, SV, length(md), length(rd))
end

COBREXA.objective(m::mdl) = sparse([m.rxns[rid].oc for rid in reactions(m)])

COBREXA.bounds(m::mdl) = (
    sparse([m.rxns[rid].lb for rid in reactions(m)]),
    sparse([m.rxns[rid].ub for rid in reactions(m)]),
)

t = @elapsed save_json_model(mdl(rxns, mets, cmets), "data/model-$name.json")
log("save_json", t, filesize("data/model-$name.json"))
t = @elapsed save_mat_model(mdl(rxns, mets, cmets), "data/model-$name.mat")
log("save_mat", t, filesize("data/model-$name.mat"))
